def codeToMorse(words):
    code={"A": "._",  "B": "_...","C": "_._.","D": "_..",
          "E": ".",   "F": ".._.","G": "__.", "H": "....",
          "I": "..",  "J": ".___","K": "_._", "L": "._..",
          "M": "__",  "N": "_.",  "O": "___", "P": ".__.",
          "Q": "__._","R": "._.", "S": "...", "T": "_",
          "U": ".._", "V": "..._","W": ".__", "X": "_.._",
          "Y": "_.__","Z": "__..","1": ".____", "6": "_....",
          "2": "..___", "7": "__...","3": "...__", "8": "___..",
          "4": "...._", "9": "____.","5": ".....", "0": "_____"}
    word = ""
    for i in words:
        if i.upper() in code:
            word += code[i.upper()] + " "
    print(word)
def codeToHuman(words):
    code={"A": "._",  "B": "_...","C": "_._.","D": "_..",
          "E": ".",   "F": ".._.","G": "__.", "H": "....",
          "I": "..",  "J": ".___","K": "_._", "L": "._..",
          "M": "__",  "N": "_.",  "O": "___", "P": ".__.",
          "Q": "__._","R": "._.", "S": "...", "T": "_",
          "U": ".._", "V": "..._","W": ".__", "X": "_.._",
          "Y": "_.__","Z": "__..","1": ".____", "6": "_....",
          "2": "..___", "7": "__...","3": "...__", "8": "___..",
          "4": "...._", "9": "____.","5": ".....", "0": "_____"}
    code_swap = {v: k for k, v in code.items()}
    word = ""
    for i in words.split(" "):
        if i in code_swap:
            word += code_swap[i] + " "
    print(word.lower())

while True:
    i = input("Press 1 if you want to code to Morse,\nPress 2 if you want to decode from Morse \n")
    if i == "1" or i == "2":
        break
    else:
        print("Invalid input")
if i == "1":
    codeToMorse(input("Introduceti textul pentru codare: "))
else:
    codeToHuman(input("Introduceti textul pentru decodare: "))










