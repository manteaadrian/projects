import random

suits = ('Hearts', 'Diamonds', 'Spades', 'Clubs')
ranks = ('Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Jack', 'Queen', 'King', 'Ace')
values = {'Two':2, 'Three':3, 'Four':4, 'Five':5, 'Six':6, 'Seven':7, 'Eight':8,'Nine':9, 'Ten':10, 'Jack':12, 'Queen':13, 'King':14, 'Ace':11}

class CarteJoc:

    def __init__(self,suit,rank):
        self.suit = suit
        self.rank = rank

    def __str__(self):
        return self.rank + " of " + self.suit

class Pachet:

    def __init__(self):
        self.all_cards = []
        for suit in suits:
            for rank in ranks:
                self.all_cards.append(CarteJoc(suit,rank))

    def amestec(self):
        return random.shuffle(self.all_cards)

    def arde(self):
        return self.all_cards.pop()

class Masa:

    def __init__(self):
        self.mana_jucator = []
        self.mana_masa = []
        if len(self.mana_masa) <2 and len(self.mana_jucator) <2:
            for i in range(0,2):
                self.mana_jucator.append(pachet.all_cards[-1])
                pachet.arde()
                self.mana_masa.append(pachet.all_cards[-1])
                pachet.arde()

    def calcul1(self):
        self.s = 0
        for i in self.mana_jucator:
            i = str(i).split()
            self.s = self.s + values[i[0]]
        return self.s

    def calcul2(self):
        self.ss = 0
        for i in self.mana_masa:
            i = str(i).split()
            self.ss = self.ss + values[i[0]]
        print(self.ss)

    def alegere(self):
        d = input("Vrei sa primesti o carte noua sau te opresti? (Y/N) ")
        if d == 'Y':
            self.mana_jucator.append(pachet.all_cards[-1])
            pachet.arde()
            return True
        if d == "N":
            return "N"


pachet = Pachet()
pachet.amestec()
masa = Masa()
while True:
    if int(masa.calcul1()) > 21:
        for i in range(0, len(masa.mana_jucator)):
            print(masa.mana_jucator[i])
        print(masa.calcul1())
        print("Ai pierdut")
        break
    for i in range(0, len(masa.mana_jucator)):
        print(masa.mana_jucator[i])
    print(masa.calcul1())
    verificare = masa.alegere()
    if verificare == "N":
        pass
